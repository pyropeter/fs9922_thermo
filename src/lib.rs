use nb::Error::{Other,WouldBlock};
use nb;
use tinyvec::ArrayVec;

const MESSAGE_LEN: usize = 12;
const FRAME_LEN:   usize = MESSAGE_LEN + 2;

#[derive(Debug, PartialEq, Eq)]
pub enum Error {
    NotCelsiusError,
    OverLimitsError,
    SyncLostError,
    SyntaxError,
}

fn parse_digit(c: u8) -> Result<i16, Error> {
    if b'0' > c || c > b'9' {
        Err(Error::SyntaxError)
    } else {
        Ok((c - b'0').into())
    }
}

fn parse_four_digits(cs: &[u8]) -> Result<i16, Error> {
    if cs.len() != 4 {
        panic!("parse_four_digits needs a slice of length four");
    } else {
        Ok( parse_digit(cs[0])? * 1000 +
            parse_digit(cs[1])? * 100 +
            parse_digit(cs[2])? * 10 +
            parse_digit(cs[3])?)
    }
}

fn parse(buf: &[u8]) -> Result<i16, Error> {
    if buf.len() != MESSAGE_LEN {
        panic!("buf.len() must be MESSAGE_LEN");
    } else if &buf[1..5] == b"?0:?" {
        return Err(Error::OverLimitsError);
    }

    let negative   = buf[0] == b'-';
    let value      = parse_four_digits(&buf[1..5])?;
    let pointpos   = parse_digit(buf[6])?;

    if buf[5] != b' ' {
        Err(Error::SyntaxError)
    } else if !negative && buf[0] != b'+' {
        Err(Error::SyntaxError)
    } else if buf[10] != 2 {
        Err(Error::NotCelsiusError)
    } else if buf[7] & 0xf9 != 0 {
        Err(Error::SyntaxError)
    } else if buf[8] & 0x02 != 0 {
        Err(Error::SyntaxError)
    } else if buf[9] & 0xfe != 0 {
        Err(Error::SyntaxError)
    } else if pointpos != 0 {
        Err(Error::SyntaxError)
    } else {
        let mut val: i16 = value.into();

        if negative {
            val = -val;
        }

        Ok(val)
    }
}

#[derive(Debug, Default)]
pub struct Thermometer {
    buf: ArrayVec<[u8; FRAME_LEN]>,
}

impl Thermometer {
    pub fn new() -> Thermometer {
        Default::default()
    }

    pub fn feed(&mut self, b: u8) -> nb::Result<i16, Error> {
        self.buf.push(b);

        if self.buf.len() == FRAME_LEN {
            if self.buf[FRAME_LEN-2] == b'\r' && self.buf[FRAME_LEN-1] == b'\n' {
                // probably received valid frame
                let result = parse(&self.buf[..MESSAGE_LEN]);
                self.buf.clear();
                result.map_err(Other)
            } else {
                // synchronisation lost
                match self.buf.iter().position(|&b| b == b'\n') {
                    None      =>   self.buf.clear(),
                    Some(pos) => { self.buf.drain(0..pos+1); },
                }

                Err(Other(Error::SyncLostError))
            }
        } else {
            Err(WouldBlock)
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sync_and_parse() {
        const INPUT: &[u8; 22] = b"abc\n\0\0\r\n+1234 0\0\0\0\x02\0\r\n";

        let mut s = Thermometer::new();
        let mut i = INPUT.iter();

        assert_eq!(nb::block!({
            s.feed(*i.next().unwrap())
        }), Err(Error::SyncLostError));

        assert_eq!(nb::block!({
            s.feed(*i.next().unwrap())
        }), Err(Error::SyncLostError));

        assert_eq!(nb::block!({
            s.feed(*i.next().unwrap())
        }), Ok(1234));
    }
}
