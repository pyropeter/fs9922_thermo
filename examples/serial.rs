use fs9922_thermo::Thermometer;
use nb::Error::{Other,WouldBlock};
use serial::SerialPort;
use serial;
use std::env;
use std::io::Read;
use std::time::Duration;

fn main() {
    let mut port = serial::open(&env::args_os().skip(1).next().unwrap()).unwrap();

    port.reconfigure(&|settings| {
        settings.set_baud_rate(serial::Baud2400)?;
        settings.set_char_size(serial::Bits8);
        settings.set_parity(serial::ParityNone);
        settings.set_stop_bits(serial::Stop1);
        settings.set_flow_control(serial::FlowNone);
        Ok(())
    }).unwrap();

    port.set_timeout(Duration::from_secs(10)).unwrap();

    let mut thermometer = Thermometer::new();

    for byte in port.bytes() {
        match thermometer.feed(byte.unwrap()) {
            Ok(temp)        => println!("Temperature: {}°C", temp),
            Err(WouldBlock) => (),
            Err(Other(e))   => println!("{:?}", e),
        }
    }
}

